import React from 'react';
import './style.css';
import './main.js';

import { useHistory } from 'react-router-dom';

export default function Fredirector() {
  let history = useHistory();
  function re() {
    history.push('/login');
  }
  return (
    <>
      <h2 onClick={re()}>YOU HAVE BEEN REDIRECTED</h2>
    </>
  );
}
