import React from "react";
import "./style.css";
import "./main.js";

import { Link } from 'react-router-dom'

export default function Log_in_component(){
  return (
    <>
    
    <title>LOG IN</title>
    <div className="wrapper">
      <label>© PLAY QUIZ WITH FRIENDS </label>
    </div>
    <div className="wrapper">
      <div className="title">
        LOG IN
      </div>
      <div className="form">
        <div className="inputfield">
          <input type="email" className="input" placeholder="EMAIL"></input>
        </div>
        <div className="inputfield">
          <input type="email" className="input"  placeholder="PASSWORD"></input>
        </div>
        <div className="inputfield">
        <Link to="/signup" style={{ textDecoration: 'none' }}><h2 className="LOOGER"  >DON'T HAVE A ACCOUNT? SIGN UP</h2></Link>
        </div>
      </div>
      <div className="inputfield">
          <button className="sbbtn">LOGIN</button>
        </div>
    </div>
    </>
  )
}